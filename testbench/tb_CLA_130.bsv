/*
see LICENSE.iitm

Author : Nagakaushik Moturi
Email id : ee17b111@smail.iitm.ac.in
Details: testbench for Carry look ahead adder using + as the golden version.

--------------------------------------------------------------------------------------------------
*/
package tb_CLA_130;
  import CLA_130 ::*;
  import Randomizable ::*;
  
   module mk_tb_CLA();
    Reg#(Bit#(32)) cycle <- mkReg(0);
    Reg#(Bit#(32)) count <- mkReg(0);
    Reg#(Bit#(132)) feed <- mkReg(0);
    Reg#(Bit#(130)) rand1 <- mkReg(0);
    Reg#(Bit#(130)) rand2 <- mkReg(0);
    Reg#(Bit#(130)) prod <- mkReg(0);
    Reg#(Bit#(130)) prod1 <- mkReg(0);
    Reg#(Bit#(1)) valid <- mkReg(0);
        
    Ifc_CLA ifc <- mk_CLA(); 
    
    Randomize#(Bit#(130)) rand_in1 <- mkConstrainedRandomizer(130'd0,130'h3ffffffffffffffffffffffffffffffff);
    Randomize#(Bit#(130)) rand_in2 <- mkConstrainedRandomizer(130'd0,130'h3ffffffffffffffffffffffffffffffff);
    
    
    rule init(feed == 0);            //initializing the 2 random values
      rand_in1.cntrl.init();
      rand_in2.cntrl.init();
      feed <= 1;
    endrule
    
    rule rl_stage1(feed==1);              
      let a <- rand_in1.next();
      let b <- rand_in2.next();
      rand1 <= a;
      rand2 <= b;
      ifc.send(rand1,rand2);
    endrule
 
    
    rule receive;   //Using the recieve method to receive the outputs and displaying them
      match {.out,.out1} = ifc.receive();
      
      if (out!=out1) count <= count+1;

      if (feed==1) $display("Cycle %d : Valid %d : Out %d,%d, MUL",cycle,valid,out,out1);
      if (feed==1) $display("Cycle %d : Valid %d : Out %b MUL",cycle,valid,(out==out1));
      
      if (cycle==200001) $display("%d failed cases",count);
    endrule
    
    rule cycling;          //generating cycles
      cycle <= cycle +1;
      if(cycle>200000) begin
        $finish(0);
        end
    endrule
    
  endmodule
endpackage
