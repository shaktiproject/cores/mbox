/*
see LICENSE.iitm

Author : Nagakaushik Moturi
Email id : ee17b111@smail.iitm.ac.in
Details: testbench for pipelined multiplier using the baseline multiplier.

--------------------------------------------------------------------------------------------------
*/
package tb_int_multiplier;
 import int_multiplier ::*;
 import int_multiplier_pipelined_4stages ::*;
 import Randomizable ::*;
 `include "Logger.bsv"
 `include "mbox_parameters.bsv"
  module mk_tb_int_multiplier();
    Reg#(Bit#(32)) cycle <- mkReg(0);
    Reg#(Bit#(32)) count <- mkReg(0);
    Reg#(Bit#(32)) feed <- mkReg(0);
    Reg#(Bit#(64)) rand1 <- mkReg(0);
    Reg#(Bit#(64)) rand2 <- mkReg(0);
    
    int_multiplier::Ifc_int_multiplier ifc <- int_multiplier::mk_int_multiplier();  //instantiating the baseline multiplier and using it as the golden version
    int_multiplier_pipelined_4stages::Ifc_int_multiplier ifc1 <- int_multiplier_pipelined_4stages::mk_int_multiplier();  //instantiating the pipelined multiplier
    
    Randomize#(Bit#(64)) rand_in1 <- mkConstrainedRandomizer(64'd0,64'hffffffffffffffff);
    Randomize#(Bit#(64)) rand_in2 <- mkConstrainedRandomizer(64'd0,64'hffffffffffffffff);
    
    rule init(feed == 0);            //initializing the 2 random values
      rand_in1.cntrl.init();
      rand_in2.cntrl.init();
      feed <= 1;
    endrule
    
    rule rl_stage1(feed==1);              //feeding random inputs in MUL mode to both baseline multiplier and pipelined version
      let a <- rand_in1.next();
      let b <- rand_in2.next();
      rand1 <= a;
      rand2 <= b;
      ifc.send(rand1,rand2,3'b000 `ifdef RV64 ,False `endif );
      ifc1.send(rand1,rand2,3'b000 `ifdef RV64 ,False `endif );
      feed<=2;
    endrule
    rule rl_stage2(feed==2);              //feeding random inputs in MULH mode to both baseline multiplier and pipelined version
      ifc.send(rand1,rand2,3'b001 `ifdef RV64 ,False `endif );
      ifc1.send(rand1,rand2,3'b001 `ifdef RV64 ,False `endif );
      feed<=3;
    endrule
    rule rl_stage3(feed==3);              //feeding random inputs in MULHSU mode to both baseline multiplier and pipelined version
      ifc.send(rand1,rand2,3'b010 `ifdef RV64 ,False `endif );
      ifc1.send(rand1,rand2,3'b010 `ifdef RV64 ,False `endif );
      feed<=4;
    endrule
    rule rl_stage4(feed==4);             //feeding random inputs in MULHU mode to both baseline multiplier and pipelined version
      ifc.send(rand1,rand2,3'b011 `ifdef RV64 ,False `endif );
      ifc1.send(rand1,rand2,3'b011 `ifdef RV64 ,False `endif );
      feed<=5;
    endrule
    rule rl_stage5(feed==5);             //feeding random inputs in MULW mode to both baseline multiplier and pipelined version
      ifc.send(rand1,rand2,3'b000 `ifdef RV64 ,True `endif );
      ifc1.send(rand1,rand2,3'b000 `ifdef RV64 ,True `endif );
      feed<=1;
    endrule
      
    rule receive;   //Using the recieve method to receive the outputs and displaying them
      match {.valid,.out} = ifc.receive();
      match {.valid1,.out1} = ifc1.receive();
      
      if (out!=out1) count <= count+1;
      
      if (feed==5) $display("Cycle %d : Valid %d : Out %b, MUL",cycle,valid,(out==out1));
      if (feed==1) $display("Cycle %d : Valid %d : Out %b, MULH",cycle,valid,(out==out1));
      if (feed==2) $display("Cycle %d : Valid %d : Out %b, MULHSU",cycle,valid,(out==out1));
      if (feed==3) $display("Cycle %d : Valid %d : Out %b, MULHU",cycle,valid,(out==out1));
      if (feed==4) $display("Cycle %d : Valid %d : Out %b, MULW",cycle,valid,(out==out1));
      
      if (feed==5) $display("Cycle %d : Valid %d : Out %d,%d, MUL",cycle,valid,out,out1);
      if (feed==1) $display("Cycle %d : Valid %d : Out %d,%d, MULH",cycle,valid,out,out1);
      if (feed==2) $display("Cycle %d : Valid %d : Out %d,%d, MULHSU",cycle,valid,out,out1);
      if (feed==3) $display("Cycle %d : Valid %d : Out %d,%d, MULHU",cycle,valid,out,out1);
      if (feed==4) $display("Cycle %d : Valid %d : Out %d,%d MULW",cycle,valid,out,out1);
      
      if (cycle==200001) $display("%d failed cases",count);
    endrule
    
    rule cycling;          //generating cycles
      cycle <= cycle +1;
      if(cycle>200000)
          $finish(0);
    endrule
    
  endmodule
endpackage
