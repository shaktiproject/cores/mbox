# Simple tests for an adder module
import cocotb
from mkintegerModel import divider_model
import random
from cocotb.clock import Clock
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge, ReadOnly, FallingEdge
from cocotb.monitors import Monitor
from cocotb.drivers import BitDriver
from cocotb.binary import BinaryValue
from cocotb.regression import TestFactory
from cocotb.scoreboard import Scoreboard
from cocotb.result import TestFailure, TestSuccess

#------------------------------------Test for signed Division------------------------------------------------------
r = 1000
@cocotb.test()
def divider_basic_signed_DIV_test(dut):  #14
    cocotb.fork(Clock(dut.CLK, 10,).start())
    divName = 14   #fixed..
    for it in range(r):
        if(it==0):
            A = -15
            B = 5
        elif(it==1):
            A = -18
            B = 0
        elif(it==2):
            A = 9223372036854775807
            B = random.randrange(-9223372036854775808,9223372036854775807,1000)
        elif(it==3):
            A = -9223372036854775808
            B = random.randrange(-9223372036854775808,9223372036854775807,1000)
        elif(it==4):
            A = 15
            B = 15
        else:
            A = random.randrange(-9223372036854775808,9223372036854775807,1000)
            B = random.randrange(-9223372036854775808,9223372036854775807,1000)

        clkedge = RisingEdge(dut.CLK)
        
        dut.ma_set_flush_c = 1
        dut.EN_ma_set_flush = 1
        dut.RST_N = 0
        for i in range(5):
            yield clkedge
        
        dut.EN_ma_start = 1
        dut.RST_N = 1
        dut.ma_start_div_name = BinaryValue(value=divName,bits=5,bigEndian=False)
        dut.ma_start_dividend = BinaryValue(value=A,bits=64,bigEndian=False,binaryRepresentation=2)
        dut.ma_start_divisor = BinaryValue(value=B,bits=64,bigEndian=False,binaryRepresentation=2)
        yield clkedge
        
        dut.EN_ma_start = 0        
        while((dut.RDY_mav_result)!=1):
            yield clkedge
        
#        dutResultBin = "".join(reversed(str(dut.mav_result)))
#        modelResultBin = "".join(reversed(str(divider_model(A,B,divName))))     

        dutResultBin = "".join((str(dut.mav_result)))
        modelResultBin = "".join((str(divider_model(A,B,divName))))     
        
        dut.RST_N = 1
        for i in range(5):
            yield clkedge
        
        if(modelResultBin != dutResultBin):
            print("Value from DUT is not and Model are not equal:")
            print("A: ",A)
            print("B: ",B)
            print(modelResultBin,"<========Model========")
            print(dutResultBin,"<========DUT========")
            raise TestFailure("Incorrect Signed Division result")
        elif(it==0):
            dut.log.info("Basic Signed Division for (-15/5) Test Passed..") 
        else:
            dut.log.info("OK!") 
    

# ------------------------------------Test for unsigned Division------------------------------------------------------

@cocotb.test()
def divider_basic_unsigned_DIV_test(dut):  #15
    cocotb.fork(Clock(dut.CLK, 10,).start())
    divName = 15   #fixed..
    for it in range(r):
        if(it==0):
            A = 15
            B = 5
        elif(it==1):
            A = 18
            B = 0
        elif(it==2):
            A = 18446744073709551615
            B = random.randrange(0,18446744073709551615,100)
        elif(it==3):
            A = 0
            B = random.randrange(0,18446744073709551615,100)
        elif(it==4):
            A = 15
            B = 15
        else:
            A = random.randrange(0,18446744073709551615,100)
            B = random.randrange(0,18446744073709551615,100)
            
        
        
        clkedge = RisingEdge(dut.CLK)
        
        dut.ma_set_flush_c = 1
        dut.EN_ma_set_flush = 1
        dut.RST_N = 0
        for i in range(5):
            yield clkedge
        
        dut.EN_ma_start = 1
        dut.RST_N = 1
        dut.ma_start_div_name = BinaryValue(value=divName,bits=5,bigEndian=False)
        dut.ma_start_dividend = BinaryValue(value=A,bits=64,bigEndian=False)
        dut.ma_start_divisor = BinaryValue(value=B,bits=64,bigEndian=False)
        yield clkedge
        
        dut.EN_ma_start = 0        
        while((dut.RDY_mav_result)!=1):
            yield clkedge
       
#        dutResultBin = "".join(reversed(str(dut.mav_result)))
#        modelResultBin = "".join(reversed(str(divider_model(A,B,divName))))     

        dutResultBin = "".join((str(dut.mav_result)))
        modelResultBin = "".join((str(divider_model(A,B,divName))))     
       
        dut.RST_N = 1
        for i in range(5):
            yield clkedge
        
        if(modelResultBin != dutResultBin):
            print("Value from DUT is not and Model are not equal:")
            print(modelResultBin,"<========Model========")
		# print("A: ",A)
		# print("B: ",B)
            print(dutResultBin,"<========DUT========")
            raise TestFailure("Incorrect Unsigned Division result")
        elif(it==0):
            dut.log.info("Basic Unsigned Division for (-15/5) Test Passed..") 
        else:
            dut.log.info("OK!") 
        

# -----------------------------------------Test for signed Division (Remainder)-------------------------------------------------
@cocotb.test()
def divider_basic_signed_REM_test(dut):  #16
    cocotb.fork(Clock(dut.CLK, 10,).start())
    divName = 16   #fixed..
    for it in range(r):
        if(it==0):
            A = -15
            B = 4
        elif(it==1):
            A = -18
            B = 0
        elif(it==2):
            A = 9223372036854775807
            B = random.randrange(-9223372036854775808,9223372036854775807,1000)
        elif(it==3):
            A = -9223372036854775808
            B = random.randrange(-9223372036854775808,9223372036854775807,1000)
        elif(it==4):
            A = 15
            B = 15
        else:
            A = random.randrange(-9223372036854775808,9223372036854775807,1000)
            B = random.randrange(-9223372036854775808,9223372036854775807,1000)

        clkedge = RisingEdge(dut.CLK)
        
        dut.ma_set_flush_c = 1
        dut.EN_ma_set_flush = 1
        dut.RST_N = 0
        for i in range(5):
            yield clkedge
        
        dut.EN_ma_start = 1
        dut.RST_N = 1
        dut.ma_start_div_name = BinaryValue(value=divName,bits=5,bigEndian=False)
        dut.ma_start_dividend = BinaryValue(value=A,bits=64,bigEndian=False,binaryRepresentation=2)
        dut.ma_start_divisor = BinaryValue(value=B,bits=64,bigEndian=False,binaryRepresentation=2)
        yield clkedge
        
        dut.EN_ma_start = 0        
        
        while((dut.RDY_mav_result)!=1):
            yield clkedge
        
#        dutResultBin = "".join(reversed(str(dut.mav_result)))
#        modelResultBin = "".join(reversed(str(divider_model(A,B,divName))))     

        dutResultBin = "".join((str(dut.mav_result)))
        modelResultBin = "".join((str(divider_model(A,B,divName))))     
        
        dut.RST_N = 1
        for i in range(5):
            yield clkedge
        
        if(modelResultBin != dutResultBin):
            print("Value from DUT is not and Model are not equal:")
            print("A: ",A)
            print("B: ",B)
            print(modelResultBin,"<========Model========")
		# print("A: ",A)
		# print("B: ",B)
            print(dutResultBin,"<========DUT========")
            raise TestFailure("Incorrect Signed Remainder result")
        elif(it==0):
            dut.log.info("Basic Signed Remainder for (-15/5) Test Passed..") 
        else:
            dut.log.info("OK!") 
        
# -----------------------------------------Test for unsigned Division (Remainder)-------------------------------------------------

@cocotb.test()
def divider_basic_unsigned_REM_test(dut):  #17
    cocotb.fork(Clock(dut.CLK, 10,).start())
    divName = 17   #fixed..
    for it in range(r):
        if(it==0):
            A = 15
            B = 4
        elif(it==1):
            A = 18
            B = 0
        elif(it==2):
            A = 18446744073709551615
            B = random.randrange(0,18446744073709551615,100)
        elif(it==3):
            A = 0
            B = random.randrange(0,18446744073709551615,100)
        elif(it==4):
            A = 15
            B = 15
        else:
            A = random.randrange(0,18446744073709551615,100)
            B = random.randrange(0,18446744073709551615,100)
          
        
        clkedge = RisingEdge(dut.CLK)
        
        dut.ma_set_flush_c = 1
        dut.EN_ma_set_flush = 1
        dut.RST_N = 0
        for i in range(5):
            yield clkedge
        
        dut.EN_ma_start = 1
        dut.RST_N = 1
        dut.ma_start_div_name = BinaryValue(value=divName,bits=5,bigEndian=False)
        dut.ma_start_dividend = BinaryValue(value=A,bits=64,bigEndian=False)
        dut.ma_start_divisor = BinaryValue(value=B,bits=64,bigEndian=False)
        yield clkedge

        dut.EN_ma_start = 0        

        while((dut.RDY_mav_result)!=1):
            yield clkedge

#        dutResultBin = "".join(reversed(str(dut.mav_result)))
#        modelResultBin = "".join(reversed(str(divider_model(A,B,divName))))     

        dutResultBin = "".join((str(dut.mav_result)))
        modelResultBin = "".join((str(divider_model(A,B,divName))))     
        
        dut.RST_N = 1
        for i in range(5):
            yield clkedge
        
        if(modelResultBin != dutResultBin):
            print("Value from DUT is not and Model are not equal:")
            print("A: ",A)
            print("B: ",B)
            print(modelResultBin,"<========Model========")
		# print("A: ",A)
		# print("B: ",B)
            print(dutResultBin,"<========DUT========")
            raise TestFailure("Incorrect Unsigned Remainder result")
        elif(it==0):
            dut.log.info("Basic Unsigned Remainder for (-15/5) Test Passed..") 
        else:
            dut.log.info("OK!") 



#-----------------------------------------Test for signed Division 32bits---------------------------------------------

@cocotb.test()
def divider_basic_signed_DIV_test_32(dut):  #18
    cocotb.fork(Clock(dut.CLK, 10,).start())
    divName = 18   #fixed..
    for it in range(r):
        if(it==0):
            A = -15
            B = 5
        elif(it==1):
            A = -18
            B = 0
        elif(it==2):
            A = 2147483647
            B = random.randrange(-2147483648,2147483647,1000)
        elif(it==3):
            A = -2147483648
            B = random.randrange(-2147483648,2147483647,1000)
        elif(it==4):
            A = 15
            B = 15
        else:
            A = random.randrange(-2147483648,2147483647,1000)
            B = random.randrange(-2147483648,2147483648,1000)

        clkedge = RisingEdge(dut.CLK)
        
        dut.ma_set_flush_c = 1
        dut.EN_ma_set_flush = 1
        dut.RST_N = 0
        for i in range(5):
            yield clkedge
        
        dut.EN_ma_start = 1
        dut.RST_N = 1
        dut.ma_start_div_name = BinaryValue(value=divName,bits=5,bigEndian=False)
        dut.ma_start_dividend = BinaryValue(value=A,bits=64,bigEndian=False,binaryRepresentation=2)
        dut.ma_start_divisor = BinaryValue(value=B,bits=64,bigEndian=False,binaryRepresentation=2)
        yield clkedge
        
        dut.EN_ma_start = 0        
        while((dut.RDY_mav_result)!=1):
            yield clkedge
        
#        dutResultBin = "".join(reversed(str(dut.mav_result)))
#        modelResultBin = "".join(reversed(str(divider_model(A,B,divName))))     

        dutResultBin = "".join((str(dut.mav_result)))
        modelResultBin = "".join((str(divider_model(A,B,divName))))     
        
        dut.RST_N = 1
        for i in range(5):
            yield clkedge
        
        if(modelResultBin != dutResultBin):
            print("Value from DUT is not and Model are not equal:")
            print("A: ",A)
            print("B: ",B)
            print(modelResultBin,"<========Model========")
            print(dutResultBin,"<========DUT========")
            raise TestFailure("Incorrect Signed Division 32bits result")
        elif(it==0):
            dut.log.info("Basic Signed Division 32bits for (-15/5) Test Passed..") 
        else:
            dut.log.info("OK!") 

# -------------------------------------Test for Unsigned Division 32bits------------------------------------
@cocotb.test()
def divider_basic_unsigned_DIV_test_32(dut):  #19
    cocotb.fork(Clock(dut.CLK, 10,).start())
    divName = 19   #fixed..
    for it in range(r):
        if(it==0):
            A = 15
            B = 5
        elif(it==1):
            A = 18
            B = 0
        elif(it==2):
            A = 4294967295
            B = random.randrange(0,4294967295,100)
        elif(it==3):
            A = 0
            B = random.randrange(0,4294967295,100)
        elif(it==4):
            A = 15
            B = 15
        else:
            A = random.randrange(0,4294967295,100)
            B = random.randrange(0,4294967295,100)
            
        
        
        clkedge = RisingEdge(dut.CLK)
        
        dut.ma_set_flush_c = 1
        dut.EN_ma_set_flush = 1
        dut.RST_N = 0
        for i in range(5):
            yield clkedge
        
        dut.EN_ma_start = 1
        dut.RST_N = 1
        dut.ma_start_div_name = BinaryValue(value=divName,bits=5,bigEndian=False)
        dut.ma_start_dividend = BinaryValue(value=A,bits=64,bigEndian=False)
        dut.ma_start_divisor = BinaryValue(value=B,bits=64,bigEndian=False)
        yield clkedge
        
        dut.EN_ma_start = 0        
        while((dut.RDY_mav_result)!=1):
            yield clkedge
       
#        dutResultBin = "".join(reversed(str(dut.mav_result)))
#        modelResultBin = "".join(reversed(str(divider_model(A,B,divName))))     

        dutResultBin = "".join((str(dut.mav_result)))
        modelResultBin = "".join((str(divider_model(A,B,divName))))     
       
        dut.RST_N = 1
        for i in range(5):
            yield clkedge
        
        if(modelResultBin != dutResultBin):
            print("Value from DUT is not and Model are not equal:")
            print(modelResultBin,"<========Model========")
            print("A: ",A)
            print("B: ",B)
            print(dutResultBin,"<========DUT========")
            raise TestFailure("Incorrect Unsigned Division 32bits result")
        elif(it==0):
            dut.log.info("Basic Unsigned Division 32bits for (-15/5) Test Passed..") 
        else:
            dut.log.info("OK!")  


# -----------------------------------------Test for signed Division (Remainder) 32bits-----------------------------------------------
@cocotb.test()
def divider_basic_signed_REM_test_32(dut):  #20
    cocotb.fork(Clock(dut.CLK, 10,).start())
    divName = 20   #fixed..
    for it in range(r):
        if(it==0):
            A = -15
            B = 4
        elif(it==1):
            A = -18
            B = 0
        elif(it==2):
            A = 2147483647
            B = random.randrange(-2147483648,2147483647,1000)
        elif(it==3):
            A = -2147483648
            B = random.randrange(-2147483648,2147483647,1000)
        elif(it==4):
            A = 15
            B = 15
        else:
            A = random.randrange(-2147483648,2147483647,1000)
            B = random.randrange(-2147483648,2147483647,1000)

        clkedge = RisingEdge(dut.CLK)
        
        dut.ma_set_flush_c = 1
        dut.EN_ma_set_flush = 1
        dut.RST_N = 0
        for i in range(5):
            yield clkedge
        
        dut.EN_ma_start = 1
        dut.RST_N = 1
        dut.ma_start_div_name = BinaryValue(value=divName,bits=5,bigEndian=False)
        dut.ma_start_dividend = BinaryValue(value=A,bits=64,bigEndian=False,binaryRepresentation=2)
        dut.ma_start_divisor = BinaryValue(value=B,bits=64,bigEndian=False,binaryRepresentation=2)
        yield clkedge
        
        dut.EN_ma_start = 0        
        
        while((dut.RDY_mav_result)!=1):
            yield clkedge
        
#        dutResultBin = "".join(reversed(str(dut.mav_result)))
#        modelResultBin = "".join(reversed(str(divider_model(A,B,divName))))     

        dutResultBin = "".join((str(dut.mav_result)))
        modelResultBin = "".join((str(divider_model(A,B,divName))))     
        
        dut.RST_N = 1
        for i in range(5):
            yield clkedge
        
        if(modelResultBin != dutResultBin):
            print("Value from DUT is not and Model are not equal:")
            print("A: ",A)
            print("B: ",B)
            print(modelResultBin,"<========Model========")
        # print("A: ",A)
        # print("B: ",B)
            print(dutResultBin,"<========DUT========")
            raise TestFailure("Incorrect Signed Remainder 32bits result")
        elif(it==0):
            dut.log.info("Basic Signed Remainder 32bits for (-15/5) Test Passed..") 
        else:
            dut.log.info("OK!")         


# -----------------------------------------Test for unsigned Division (Remainder)-------------------------------------------------

@cocotb.test()
def divider_basic_unsigned_REM_test_32(dut):  #21
    cocotb.fork(Clock(dut.CLK, 10,).start())
    divName = 21   #fixed..
    for it in range(r):
        if(it==0):
            A = 15
            B = 4
        elif(it==1):
            A = 18
            B = 0
        elif(it==2):
            A = 4294967295
            B = random.randrange(0,4294967295,100)
        elif(it==3):
            A = 0
            B = random.randrange(0,4294967295,100)
        elif(it==4):
            A = 15
            B = 15
        else:
            A = random.randrange(0,4294967295,100)
            B = random.randrange(0,4294967295,100)
          
        
        clkedge = RisingEdge(dut.CLK)
        
        dut.ma_set_flush_c = 1
        dut.EN_ma_set_flush = 1
        dut.RST_N = 0
        for i in range(5):
            yield clkedge
        
        dut.EN_ma_start = 1
        dut.RST_N = 1
        dut.ma_start_div_name = BinaryValue(value=divName,bits=5,bigEndian=False)
        dut.ma_start_dividend = BinaryValue(value=A,bits=64,bigEndian=False)
        dut.ma_start_divisor = BinaryValue(value=B,bits=64,bigEndian=False)
        yield clkedge

        dut.EN_ma_start = 0        

        while((dut.RDY_mav_result)!=1):
            yield clkedge

#        dutResultBin = "".join(reversed(str(dut.mav_result)))
#        modelResultBin = "".join(reversed(str(divider_model(A,B,divName))))     

        dutResultBin = "".join((str(dut.mav_result)))
        modelResultBin = "".join((str(divider_model(A,B,divName))))     
        
        dut.RST_N = 1
        for i in range(5):
            yield clkedge
        
        if(modelResultBin != dutResultBin):
            print("Value from DUT is not and Model are not equal:")
            print("A: ",A)
            print("B: ",B)
            print(modelResultBin,"<========Model========")
        # print("A: ",A)
        # print("B: ",B)
            print(dutResultBin,"<========DUT========")
            raise TestFailure("Incorrect Unsigned Remainder 32bits result")
        elif(it==0):
            dut.log.info("Basic Unsigned Remainder 32bits for (-15/5) Test Passed..") 
        else:
            dut.log.info("OK!")
#======================================================END=================================================
